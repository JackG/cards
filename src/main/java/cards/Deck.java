package cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by jacques on 31/03/16.
 */
public class Deck {

    private List<Card> cards;

    public Deck() {
        cards = new ArrayList<Card>();
        for (Color color : Color.values()) {
            for (Suite suite : Suite.values()) {
                Card card = new Card(color, suite);
                cards.add(card);
            }
        }
    }

    public int size() {
        return cards.size();
    }

    public Card getCard(int i) {
        Card[] cardsArray = cards.toArray(new Card[cards.size()]);
        return cardsArray[i];
    }

    public boolean add(Card card) {
        if (!cards.contains(card)) {
            return cards.add(card);
        }
        return false;
    }

    public void shuffle() {
        Collections.shuffle(cards);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Deck deck = (Deck) o;

        return cards.equals(deck.cards);

    }

    @Override
    public int hashCode() {
        return cards.hashCode();
    }

    @Override
    public String toString() {
        return "Deck{" +
                "cards=" + cards +
                '}';
    }
}
