package cards;

/**
 * Created by jacques on 31/03/16.
 */
public enum Suite {

    SEVEN, HEIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
}
