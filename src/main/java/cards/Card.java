package cards;

/**
 * Created by jacques on 31/03/16.
 */
public class Card implements Comparable<Card> {

    private Color color;
    private Suite suite;

    public Card(Color color, Suite suite) {
        this.color = color;
        this.suite = suite;
    }

    public Color getColor() {
        return color;
    }

    public Suite getSuite() {
        return suite;
    }

    public int compareTo(Card o) {
        int compareColor = color.compareTo(o.color);
        if (compareColor != 0) {
            return compareColor;
        }
        return suite.compareTo(o.suite);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Card card = (Card) o;

        if (color != card.color) return false;
        return suite == card.suite;

    }

    @Override
    public int hashCode() {
        int result = color.hashCode();
        result = 31 * result + suite.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Card{" +
                "color=" + color +
                ", suite=" + suite +
                '}';
    }
}
