package cards;

/**
 * Created by jacques on 31/03/16.
 */
public enum Color {

    CLUBS, DIAMONDS, HEARTS, SPADES
}
