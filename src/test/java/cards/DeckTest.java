package cards;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by jacques on 31/03/16.
 */
public class DeckTest {

    private Deck initialDeck;

    @Before
    public void setUp() {
        initialDeck = new Deck();
    }

    @Test
    public void deckHas32CardsTest() {
        assertThat(initialDeck.size(), is(32));
    }

    @Test
    public void firstCardIsClubsOfSevenTest() {
        Card card = initialDeck.getCard(0);
        assertThat(card.getColor(), is(Color.CLUBS));
        assertThat(card.getSuite(), is(Suite.SEVEN));
    }

    @Test
    public void lastCardIsSpadesOfAceTest() {
        Card card = initialDeck.getCard(initialDeck.size() - 1);
        assertThat(card.getColor(), is(Color.SPADES));
        assertThat(card.getSuite(), is(Suite.ACE));
    }

    @Test
    public void deckHasNoDuplicateCardsTest() {
        Card card = new Card(Color.CLUBS, Suite.SEVEN);
        initialDeck.add(card);
        assertThat(initialDeck.size(), is(32));
    }

    @Test
    public void decksAreEqualTest() {
        Deck deck2 = new Deck();
        assertThat(initialDeck.equals(deck2), is(true));
    }

    @Test
    public void deckIsShuffledTest() {
        Deck shuffleDeck = new Deck();
        shuffleDeck.shuffle();
        assertThat(initialDeck.equals(shuffleDeck), is(false));
    }

}
