package cards;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.hamcrest.number.OrderingComparison.lessThan;
import static org.junit.Assert.assertThat;

/**
 * Created by jacques on 31/03/16.
 */
public class CardTest {

    @Test
    public void cardsAreEqualTest() {
        Card card1 = new Card(Color.CLUBS, Suite.SEVEN);
        Card card2 = new Card(Color.CLUBS, Suite.SEVEN);
        assertThat(card1, is(card2));
    }

    @Test
    public void cardsAreNotEqualTest() {
        Card card1 = new Card(Color.CLUBS, Suite.SEVEN);
        Card card2 = new Card(Color.SPADES, Suite.ACE);
        assertThat(card1.equals(card2), is(false));
    }

    @Test
    public void clubsOfSevenIsLowerThanClubsOfHeightTest() {
        Card card1 = new Card(Color.CLUBS, Suite.SEVEN);
        Card card2 = new Card(Color.CLUBS, Suite.HEIGHT);
        assertThat(card1.compareTo(card2), lessThan(0));
    }

    @Test
    public void spadesOfSevenIsHigherThanClubsOfHeightTest() {
        Card card1 = new Card(Color.SPADES, Suite.SEVEN);
        Card card2 = new Card(Color.CLUBS, Suite.HEIGHT);
        assertThat(card1.compareTo(card2), greaterThan(0));
    }
}
